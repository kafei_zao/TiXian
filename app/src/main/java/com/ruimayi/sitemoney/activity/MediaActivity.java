package com.ruimayi.sitemoney.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.ruimayi.sitemoney.adapter.AdapterShipin;
import com.ruimayi.sitemoney.common.CommonParams;
import com.ruimayi.sitemoney.utils.MediaUtils;
import com.ruimayi.sitemoney.utils.SystemExternalFileUtils;
import com.ruimayi.sitemoney.utils.ToastUtil;
import com.ruimayi.sitemoney.views.MyListView;
import com.ruimayi.sitemoneyapp.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class MediaActivity extends BaseActivity {


    private AdapterShipin mAdapterShipin;
    @BindView(R.id.lv_shipin)
    public MyListView mShipinListView;
    @BindView(R.id.tv_shipin)
    public TextView tv_shipin;

    private List<String> videoNetworkUrls = new ArrayList<String>();
    private String strShipinParams = "";

    @Override
    public int getContentViewId() {
        return R.layout.activity_media;
    }

    @Override
    public void initActivityParams(Bundle savedInstanceState) {
        iv_head_back.setVisibility(View.VISIBLE);
        iv_head_back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tv_head_title.setText("新手教程");

        strShipinParams = getIntent().getStringExtra("data");
        initShiPinAdapter();
        showShipinInfo();
    }


    public void initShiPinAdapter(){
        if(mAdapterShipin==null){
            mAdapterShipin = new AdapterShipin(actContext);
        }
        mShipinListView.setAdapter(mAdapterShipin);
        mShipinListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String strUrl = videoNetworkUrls.get(position);
                Log.e("shipin_tag",strUrl);
                if(!TextUtils.isEmpty(strUrl) && strUrl.startsWith("http")){
                    MediaUtils.playerNetUrl(actContext,strUrl.trim());
                }else if(!TextUtils.isEmpty(strUrl) && !strUrl.startsWith("http")){
                    MediaUtils.playeVideoLocalUrl(actContext,"file://"+strUrl.trim(),"video/*");
                }else{
                    ToastUtil.show(actContext,"视频地址错误", Toast.LENGTH_LONG);
                }

            }
        });
    }

    public void showShipinInfo(){
        String strDirs =  SystemExternalFileUtils.createDirByExternal("", CommonParams.DownLoadFileParams.FILE_DOWNLOAD_DIRS);
        List<String > filesPath = null;//文件夹下所有文件
        if(!TextUtils.isEmpty(strDirs)){
            //文件夹不为空
            filesPath = SystemExternalFileUtils.getParentFiles(strDirs);
        }
        List<String> listNetData = new ArrayList<String>();
        if(!TextUtils.isEmpty(strShipinParams)){
            strShipinParams = strShipinParams.replaceAll("\"","");
            String[] strUrls = strShipinParams.split("\\,");
            if(strUrls!=null && strUrls.length>0){
                for (String msg:strUrls ) {
                    listNetData.add(msg);
                }
            }
        }
        if(videoNetworkUrls!=null){
            videoNetworkUrls.clear();
        }
       if(filesPath.size()>0 && listNetData.size()>0 && filesPath.size()==listNetData.size()){
            //使用本地
           videoNetworkUrls.addAll(filesPath);
           tv_shipin.setVisibility(View.GONE);
           mShipinListView.setVisibility(View.VISIBLE);
       }else if( listNetData.size()>0 && filesPath.size()!=listNetData.size()){
           videoNetworkUrls.addAll(listNetData);
           tv_shipin.setVisibility(View.GONE);
           mShipinListView.setVisibility(View.VISIBLE);
       }else {
           tv_shipin.setVisibility(View.VISIBLE);
           mShipinListView.setVisibility(View.GONE);
       }
        mAdapterShipin.setDatas(videoNetworkUrls);
    }
}
