package com.ruimayi.sitemoney.activity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.ruimayi.sitemoney.beans.ADSiteModel;
import com.ruimayi.sitemoneyapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 适配器
 * Created by admin on 2017/9/24.
 */

public class AdAdpter extends BaseAdapter {

    private List<ADSiteModel> datas = new ArrayList<ADSiteModel>();
    private Context mContext;

    public void setDatas(List<ADSiteModel> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        if(datas==null){
            return 0;
        }else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if(view==null){
            viewHolder = new ViewHolder();
            view =  LayoutInflater.from(mContext).inflate(R.layout.adsite_item_layout,null);
            viewHolder.setmTextView((Button) view.findViewById(R.id.tv_name));
            view.setTag(viewHolder);
        }else{
        viewHolder =(ViewHolder) view.getTag();
        }
        ADSiteModel adSiteModel = (ADSiteModel)getItem(i);
        viewHolder.getmTextView().setText(adSiteModel.getName());
        return view;
    }

    public class ViewHolder{
        private Button mTextView;

        public ViewHolder() {
        }

        public Button getmTextView() {
            return mTextView;
        }

        public void setmTextView(Button mTextView) {
            this.mTextView = mTextView;
        }
    }


}
