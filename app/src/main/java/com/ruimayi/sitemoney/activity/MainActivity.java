package com.ruimayi.sitemoney.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.ruimayi.sitemoney.VolleyUtils.HttpCallBack;
import com.ruimayi.sitemoney.VolleyUtils.HttpUtil;
import com.ruimayi.sitemoney.beans.ADSiteModel;
import com.ruimayi.sitemoney.beans.AdUserParamModel;
import com.ruimayi.sitemoney.common.CommonParams;
import com.ruimayi.sitemoney.utils.DeviceUtils;
import com.ruimayi.sitemoneyapp.R;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;


/**
 * 主页面
 */
public class MainActivity extends BaseActivity {
    @BindView(R.id.lv_ad)
    public ListView lv_ad;

    @BindView(R.id.view_progress)
    public ProgressBar view_progress;

    @BindView(R.id.btn_notice)
    public Button btn_notice;

    @BindView(R.id.btn_course)
    public Button btn_course;

    private AdAdpter adAdpter;
    private List<ADSiteModel> adNetDatas;

    private AdUserParamModel adUserParamModel;//用户信息

    @Override


    public int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    public void initActivityParams(Bundle savedInstanceState) {
        tv_head_title.setText("网站联盟");
        iv_user_info.setVisibility(View.VISIBLE);

        adAdpter = new AdAdpter();
        adAdpter.setmContext(actContext);
        adAdpter.setDatas(adNetDatas);
        lv_ad.setAdapter(adAdpter);
        lv_ad.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent mIntent = new Intent();
                        mIntent.setClass(MainActivity.this,WebViewActivity.class);
                        mIntent.putExtra("data",adNetDatas.get(i).getUrl());
                        mIntent.putExtra("title",adNetDatas.get(i).getName());
                        startActivity(mIntent);
                    }
                });
        iv_user_info.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this,AdUserActivity.class);
                intent.putExtra("datas",adUserParamModel);
                startActivity(intent);
            }
        });

        btn_notice.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this,NoticeActivity.class);
                startActivity(intent);
            }
        });

        btn_course.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(MainActivity.this,MediaActivity.class);
                intent.putExtra("data",adUserParamModel.getShipin());
                startActivity(intent);
            }
        });
        //获取网络信息
        getUserParamsByNetWork();
    }

    public void showLoginProgress(){
        if(view_progress!=null && view_progress.getVisibility()!=View.VISIBLE){
            view_progress.setVisibility(View.VISIBLE);
        }
    }
    public void hideLoginProgress(){
        if(view_progress!=null && view_progress.getVisibility()!=View.GONE){
            view_progress.setVisibility(View.GONE);
        }
    }
    /**
     * 通过网路获取用户信息
     */
    private void getUserParamsByNetWork(){
        HashMap<String,String> params = new HashMap<String,String>();
        params.put("deviceid", DeviceUtils.getImei(actContext));
        HttpUtil.getInstance().request_get(CommonParams.URL_WEB_SITE_MONEY, params, new HttpCallBack<AdUserParamModel>() {
            @Override
            public void onSuccess(AdUserParamModel data) {
                hideLoginProgress();
                adUserParamModel = data;
                if(adUserParamModel!=null && adUserParamModel.getSites()!=null && !adUserParamModel.getSites().isEmpty()){
                    if(adNetDatas!=null){
                        adNetDatas.clear();
                    }else{
                        adNetDatas = new ArrayList<ADSiteModel>();
                    }
                    adNetDatas = adUserParamModel.getSites();
                    //更新网站信息
                    adAdpter.setDatas(adNetDatas);

                }else {
                    showAlerDialog("获取网站联盟信息失败，请稍后重试");
                }
            }

            @Override
            public void onFail(String msg) {
                hideLoginProgress();
                showAlerDialog("获取网站联盟信息失败，请稍后重试");
            }
        });
    }
}
