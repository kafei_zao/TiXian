package com.ruimayi.sitemoney.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.ruimayi.sitemoney.app.MyApplication;
import com.ruimayi.sitemoneyapp.R;
import com.umeng.analytics.MobclickAgent;

import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * 基础activity
 * Created by admin on 2017/9/6.
 */

public  abstract class BaseActivity extends AppCompatActivity {
    public Context appContext;
    public Context actContext;
    private Unbinder mUnbinder;

    @BindView(R.id.iv_head_back)
    public ImageView iv_head_back;

    @BindView(R.id.tv_head_title)
    public TextView tv_head_title;

    @BindView(R.id.iv_user_info)
    public ImageView iv_user_info;

    private Dialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(getContentViewId());
        mUnbinder = ButterKnife.bind(this);
        appContext = MyApplication.getAppContext();
        actContext = this;
        initActivityParams(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mUnbinder!=null){
            mUnbinder.unbind();
        }
    }

    /**
     * 加载activity 的布局u页面
     * @return
     */
    public abstract int getContentViewId();

    /**
     * 处理activity 初始化操作
     * @param savedInstanceState
     */
    public abstract void initActivityParams(Bundle savedInstanceState);
    /**
     * 展示对话框
     * @param content
     */
    public void showAlerDialog(String content){
        new AlertDialog.Builder(this)
                .setTitle("提示")
                .setMessage(content)
                .setCancelable(false)
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create().show();
    }

    /**
     * 显示等待框
     */
    public void showLoadingDialog(String content){
        if(TextUtils.isEmpty(content)){
            content = "努力加载中...";
        }
        progressDialog = new Dialog(actContext,R.style.progress_dialog);
        progressDialog.setContentView(R.layout.dialog);
        progressDialog.setCancelable(true);
        progressDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView msg = (TextView) progressDialog.findViewById(R.id.id_tv_loadingmsg);
        msg.setText(content);
        progressDialog.show();
    }
    public void hideLoadingDialog(){
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }
}
