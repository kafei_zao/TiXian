package com.ruimayi.sitemoney.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ruimayi.sitemoney.common.CommonParams;
import com.ruimayi.sitemoney.utils.DeviceUtils;
import com.ruimayi.sitemoneyapp.R;

import butterknife.BindView;

/**
 * 石永红公告
 */
public class NoticeActivity extends BaseActivity {

    @BindView(R.id.webview_notice)
    public WebView webview_notice;



    private String strLocalHtmlPath ;//本地的网页地址
    private WebSettings mWebSetting;//webview 设置参数

    @Override
    public int getContentViewId() {
        return R.layout.activity_notice;
    }

    @Override
    public void initActivityParams(Bundle savedInstanceState) {
        showLoadingDialog("");
        iv_head_back.setVisibility(View.VISIBLE);
        iv_head_back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(webview_notice!=null && webview_notice.canGoBack()){
                    webview_notice.goBack();
                }else{
                    finish();
                }
            }
        });
        tv_head_title.setText("提现公告");
        initWebViewParams();
        strLocalHtmlPath = CommonParams.URL_WEB_NOTICE+"deviceid="+ DeviceUtils.getImei(actContext)+"&type=1";
        initLoadWebViewResources();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        clearWebViewParams();
    }
    /**
     * 初始化webview 参数
     */
    private void initWebViewParams(){
        webview_notice.setWebViewClient(new MyWebViewClient());
        webview_notice.setWebChromeClient(new MyWebChromeClient());
        mWebSetting = webview_notice.getSettings();
        mWebSetting.setJavaScriptEnabled(true);//是否支持javaScript
        webview_notice.clearHistory();
        webview_notice.clearFormData();
        webview_notice.clearCache(true);
        // mWebSetting.setCacheMode(WebSettings.LOAD_NO_CACHE);//缓存类型
        // mWebSetting.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        mWebSetting.setCacheMode(WebSettings.LOAD_DEFAULT);
        mWebSetting.setDefaultTextEncodingName("utf-8");//文件的解码
        mWebSetting.setBlockNetworkImage(false);//是否支持网络图片
//        mWebSetting.setLoadsImagesAutomatically(true);
        mWebSetting.setSupportZoom(true);//自动缩放
    }

    /**
     * webView 加载
     */
    private void initLoadWebViewResources(){
        webview_notice.loadUrl(strLocalHtmlPath);
    }


    /**
     * 清除webview 参数
     */
    private void clearWebViewParams(){
        try{
            if(webview_notice!=null){
                webview_notice.clearCache(true);
                webview_notice.clearHistory();
                webview_notice.removeAllViews();
                webview_notice.destroy();
                webview_notice=null;
            }
        }catch (Exception e){

        }
    }

    /**
     * 清除webview 系统缓存
     */
    private void clearSystemCache(){
        // 清除cookie即可彻底清除缓存
        CookieSyncManager.createInstance(actContext);
        CookieManager.getInstance().removeAllCookie();
        actContext.deleteDatabase("webview.db");
        actContext.deleteDatabase("webviewCache.db");
    }

    /**
     * 结束当前页面
     */
    private  void closedWebViewActivity(){
        clearWebViewParams();
        clearSystemCache();
        setResult(RESULT_OK);
        finish();
    }
    /**
     * 结束当前页面
     */
    private  void closedWebViewActivityError(){
        clearWebViewParams();
        clearSystemCache();
        Intent mIntent = new Intent();
        mIntent.putExtra("result","error");
        setResult(RESULT_OK,mIntent);
        finish();
    }


    /**
     * 自定义webViewClient
     * 处理各种通知、请求事件的
     */
    public class MyWebViewClient extends WebViewClient {

        public MyWebViewClient() {
            super();
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            //  super.onPageStarted(view, url, favicon);
            //判断是否加载同一条url回调
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            //页面加载完成，回调，获取url

        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //拦截url 加载回调
            view.loadUrl(url);
            return true;//返回值为true时在WebView中打开，为false时调用浏览器打开  
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            //拦截url 加载回调
            String strUrl = request.getUrl().toString().trim();
            view.loadUrl(strUrl);
            return true;//返回值为true时在WebView中打开，为false时调用浏览器打开  
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }



        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            // 回调该方法，处理SSL认证错误
        }


        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            //网页加载错误(这里进行无网络或错误处理，具体可以根据errorCode的值进行判断，做跟详细的处理。)
            //    super.onReceivedError(view, request, error);
            //结束当前加载
            closedWebViewActivity();
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            //网页加载错误(这里进行无网络或错误处理，具体可以根据errorCode的值进行判断，做跟详细的处理。)
            // super.onReceivedError(view, errorCode, description, failingUrl);
            //结束当前加载
            closedWebViewActivityError();
        }
    }


    /**
     * 主要辅助WebView处理Javascript的对话框、
     * 网站图标、网站title、
     * 加载进度等比如
     */
    public  class MyWebChromeClient extends WebChromeClient {
        public MyWebChromeClient() {
            super();
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            //通知应用程序当前网页加载的进度
            //充值handler 信息
            if(newProgress>=100){
                //页面加载完成
                hideLoadingDialog();
            }
        }


        /**
         * webview请求得到焦点，发生这个主要是当前webview不是前台状态，是后台webview。
         * @param view
         */
        @Override
        public void onRequestFocus(WebView view) {
            super.onRequestFocus(view);
        }

        @Override
        public void onCloseWindow(WebView window) {
            super.onCloseWindow(window);
            //关闭webview
        }

        /**
         * 警告框
         * @param view
         * @param url
         * @param message
         * @param result
         * @return
         */
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            result.confirm();
            return  true;
        }

        /**
         *  确定框.
         * @param view
         * @param url
         * @param message
         * @param result
         * @return
         */
        @Override
        public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
            result.confirm();
            return  true;
            //return super.onJsConfirm(view, url, message, result);
        }

        /**
         * 提示框.
         * @param view
         * @param url
         * @param message
         * @param defaultValue
         * @param result
         * @return
         */
        @Override
        public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
            result.confirm();
            return true;
            // return super.onJsPrompt(view, url, message, defaultValue, result);
        }

        @Override
        public boolean onJsBeforeUnload(WebView view, String url, String message, JsResult result) {
            return super.onJsBeforeUnload(view, url, message, result);
        }

//        /**
//         * 当前页面请求是否允许进行定位。
//         * @param origin
//         * @param callback
//         */
//        @Override
//        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
//            super.onGeolocationPermissionsShowPrompt(origin, callback);
//        }

//        /**
//         * 当前一个调用onGeolocationPermissionsShowPrompt() 取消时，通知主机应用请求地理定位权限。隐藏相关的UI。
//         */
//        @Override
//        public void onGeolocationPermissionsHidePrompt() {
//            super.onGeolocationPermissionsHidePrompt();
//        }

//        @Override
//        public void onPermissionRequest(PermissionRequest request) {
//            super.onPermissionRequest(request);
//        }

//        @Override
//        public void onPermissionRequestCanceled(PermissionRequest request) {
//            super.onPermissionRequestCanceled(request);
//        }

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            return super.onConsoleMessage(consoleMessage);
        }

//        /**
//         * Html中，视频（video）控件在没有播放的时候将给用户展示一张“海报”图片（预览图）。
//         * 其预览图是由Html中video标签的poster属性来指定的。
//         * 如果开发者没有设置poster属性, 则可以通过这个方法来设置默认的预览图。
//         * @return
//         */
//        @Override
//        public Bitmap getDefaultVideoPoster() {
//            return super.getDefaultVideoPoster();
//        }

//        /**
//         * 播放视频时，在第一帧呈现之前，需要花一定的时间来进行数据缓冲。
//         * ChromeClient可以使用这个函数来提供一个在数据缓冲时显示的视图。
//         * 例如,ChromeClient可以在缓冲时显示一个转轮动画。
//         * @return
//         */
//        @Override
//        public View getVideoLoadingProgressView() {
//            return super.getVideoLoadingProgressView();
//        }

        /**
         * 获得所有访问历史项目的列表，用于链接着色。
         * @param callback
         */
        @Override
        public void getVisitedHistory(ValueCallback<String[]> callback) {
            super.getVisitedHistory(callback);
        }

        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
        }
    }
}
