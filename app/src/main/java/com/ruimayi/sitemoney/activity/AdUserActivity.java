package com.ruimayi.sitemoney.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.ruimayi.sitemoney.beans.AdUserParamModel;
import com.ruimayi.sitemoneyapp.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import butterknife.BindView;

/**
 * 用户的个人信息页面
 * Created by admin on 2017/10/15.
 */

public class AdUserActivity extends BaseActivity {
    @BindView(R.id.tv_user_name_value)
    TextView tv_user_name_value;
    @BindView(R.id.tv_user_zhifubao_value)
    TextView tv_user_zhifubao_value;
    @BindView(R.id.tv_user_zhifubao_name_value)
    TextView tv_user_zhifubao_name_value;
    @BindView(R.id.tv_user_phone_value)
    TextView tv_user_phone_value;
    @BindView(R.id.tv_user_qq_value)
    TextView tv_user_qq_value;
    @BindView(R.id.tv_user_email_value)
    TextView tv_user_email_value;

    @BindView(R.id.tv_user_vpn_vpn_deadline_value)
    TextView tv_user_vpn_vpn_deadline_value;

    @BindView(R.id.tv_user_vpn_vpn_startdate_value)
    TextView tv_user_vpn_vpn_startdate_value;


    @BindView(R.id.tv_user_vpn_name_value)
    TextView tv_user_vpn_name_value;
    @BindView(R.id.tv_user_vpn_permonthpay_value)
    TextView tv_user_vpn_permonthpay_value;

    @BindView(R.id.tv_user_vpn_fukuan_flag)
    TextView tv_user_vpn_fukuan_flag;
    @BindView(R.id.iv_user_vpn_fukuan_value)
    ImageView iv_user_vpn_fukuan_value;

    private AdUserParamModel adUserParamModel;//用户的基本信息
    private Animation alpha ;
    @Override
    public int getContentViewId() {
        return R.layout.activity_ad_user_layout;
    }

    @Override
    public void initActivityParams(Bundle savedInstanceState) {
        iv_head_back.setVisibility(View.VISIBLE);
        iv_head_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tv_head_title.setText("用户信息");

        adUserParamModel = (AdUserParamModel) getIntent().getSerializableExtra("datas");

        Log.e("ad_param",adUserParamModel.toString());

        tv_user_name_value.setText(adUserParamModel.getUsername());
        tv_user_zhifubao_value.setText(adUserParamModel.getZhifubaoaccount());
        tv_user_zhifubao_name_value.setText(adUserParamModel.getZhifubaorealname());
        tv_user_phone_value.setText(adUserParamModel.getPhone());
        tv_user_qq_value.setText(adUserParamModel.getQq());
        tv_user_email_value.setText(adUserParamModel.getEmail());
        tv_user_vpn_vpn_deadline_value.setText(adUserParamModel.getVpndeadline());
        tv_user_vpn_vpn_startdate_value.setText(adUserParamModel.getVpnstartdate());
        tv_user_vpn_name_value.setText(adUserParamModel.getUvpnname());
        tv_user_vpn_permonthpay_value.setText(adUserParamModel.getVpnpermonthpay());
//        Bitmap mBitmap = returnBitMap(adUserParamModel.getVpnpayimage());
//        if (mBitmap != null) {
//            iv_user_vpn_fukuan_value.setImageBitmap(mBitmap);
//        }
         alpha= AnimationUtils.loadAnimation(this, R.anim.anim_alpha);//获取透明度变化动画资源
        //用户等待时设置默认图片的动画
        alpha.setRepeatCount(Animation.INFINITE);//循环显示
        iv_user_vpn_fukuan_value.startAnimation(alpha);
        loadUserVpnFuKuanImage();
    }

    private void loadUserVpnFuKuanImage(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                //从网络上获取图片
                final Bitmap bitmap=getPicture(adUserParamModel.getVpnpayimage());
                try {
                    Thread.sleep(2000);//线程休眠两秒钟
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                alpha.setRepeatCount(0);//循环显示
                //发送一个Runnable对象
                iv_user_vpn_fukuan_value.post(new Runnable(){
                    @Override
                    public void run() {
                        if(bitmap!=null && iv_user_vpn_fukuan_value!=null){
                            iv_user_vpn_fukuan_value.setImageBitmap(bitmap);//在ImageView中显示从网络上获取到的图片
                        }

                    }
                });
            }
        }).start();//开启线程

    }

    /*
   * 功能:根据网址获取图片对应的Bitmap对象
   * @param path
   * @return Bitmap
   * */
    public Bitmap getPicture(String path){
        Bitmap bm=null;
        URL url;
        try {
            url = new URL(path);//创建URL对象
            URLConnection conn=url.openConnection();//获取URL对象对应的连接
            conn.connect();//打开连接
            InputStream is=conn.getInputStream();//获取输入流对象
            bm=BitmapFactory.decodeStream(is);//根据输入流对象创建Bitmap对象
        } catch (MalformedURLException e1) {
            e1.printStackTrace();//输出异常信息
        }catch (IOException e) {
            e.printStackTrace();//输出异常信息
        }
        return bm;
    }
    /**
     * 根据url获取图片的
     * @param url
     * @return
     */
    public Bitmap returnBitMap(String url){
        URL myFileUrl = null;
        Bitmap bitmap = null;
        try {
            myFileUrl = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            HttpURLConnection conn = (HttpURLConnection) myFileUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            bitmap = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e) {
        }
        return bitmap;
    }
}
