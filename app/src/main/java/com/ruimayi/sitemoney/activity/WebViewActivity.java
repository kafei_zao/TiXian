package com.ruimayi.sitemoney.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.ruimayi.sitemoneyapp.R;


/**
 * 测试webview
 */
public class WebViewActivity extends BaseActivity {

    public WebView webView_test;

    private String strLocalHtmlPath ;//本地的网页地址
    private WebSettings mWebSetting;//webview 设置参数


    private static String Log_tag = "webview_tag";

    @Override
    public int getContentViewId() {
        return R.layout.activity_mutil_web_view;
    }

    @Override
    public void initActivityParams(Bundle savedInstanceState) {
        iv_head_back.setVisibility(View.VISIBLE);
        iv_head_back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(webView_test!=null && webView_test.canGoBack()){
                    webView_test.goBack();
                }else{
                    finish();
                }
            }
        });
        tv_head_title.setText(getIntent().getStringExtra("title"));
        webView_test = (WebView) findViewById(R.id.webview_site);
        //获取广告数据
        strLocalHtmlPath =getIntent().getStringExtra("data");
        Log.e("webview_tag","-----initActivityParams-----path------:"+strLocalHtmlPath);
        //初始化webview参数
        initWebViewParams();
        //加载webview 站点信息
        initLoadWebViewResources();
        Log.e("webview_tag","-----webviewAcivity--------initActivityParams-----------");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("webview_tag","-----onDestroy-------start----");
        clearWebViewParams();
        Log.e("webview_tag","-----onDestroy-------end----");
    }



    /**
     * 初始化webview 参数
     */
    private void initWebViewParams(){
        webView_test.setWebViewClient(new MyWebViewClient());
        webView_test.setWebChromeClient(new MyWebChromeClient());
        mWebSetting = webView_test.getSettings();
        mWebSetting.setJavaScriptEnabled(true);//是否支持javaScript
        webView_test.clearHistory();
        webView_test.clearFormData();
        webView_test.clearCache(true);
       // mWebSetting.setCacheMode(WebSettings.LOAD_NO_CACHE);//缓存类型
       // mWebSetting.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        mWebSetting.setCacheMode(WebSettings.LOAD_DEFAULT);
        mWebSetting.setDefaultTextEncodingName("utf-8");//文件的解码
        mWebSetting.setBlockNetworkImage(false);//是否支持网络图片
//        mWebSetting.setLoadsImagesAutomatically(true);
        mWebSetting.setSupportZoom(true);//自动缩放
    }

    /**
     * webView 加载
     */
    private void initLoadWebViewResources(){
        webView_test.loadUrl(strLocalHtmlPath);
    }


    /**
     * 清除webview 参数
     */
    private void clearWebViewParams(){
        try{
            if(webView_test!=null){
                webView_test.clearCache(true);
                webView_test.clearHistory();
                webView_test.removeAllViews();
                webView_test.destroy();
                webView_test=null;
            }
        }catch (Exception e){

        }
    }

    /**
     * 清除webview 系统缓存
     */
    private void clearSystemCache(){
        // 清除cookie即可彻底清除缓存
        CookieSyncManager.createInstance(actContext);
        CookieManager.getInstance().removeAllCookie();
        actContext.deleteDatabase("webview.db");
        actContext.deleteDatabase("webviewCache.db");
    }

    /**
     * 结束当前页面
     */
    private  void closedWebViewActivity(){
        Log.e(Log_tag,"---------closedWebViewActivity----------:");
        clearWebViewParams();
        clearSystemCache();
        setResult(RESULT_OK);
        finish();
    }
    /**
     * 结束当前页面
     */
    private  void closedWebViewActivityError(){
        Log.e(Log_tag,"---------closedWebViewActivityError----------:");
        clearWebViewParams();
        clearSystemCache();
        Intent mIntent = new Intent();
        mIntent.putExtra("result","error");
        setResult(RESULT_OK,mIntent);
        finish();
    }


    /**
     * 自定义webViewClient
     * 处理各种通知、请求事件的
     */
    public class MyWebViewClient extends WebViewClient{

        public MyWebViewClient() {
            super();
        }
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
          //  super.onPageStarted(view, url, favicon);
            //判断是否加载同一条url回调
            Log.e(Log_tag,"---------onPageStarted----------:"+url);
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            //页面加载完成，回调，获取url
            Log.e(Log_tag,"---------onPageFinished----------:"+url);

        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //拦截url 加载回调
            view.loadUrl(url);
            Log.e(Log_tag,"---------shouldOverrideUrlLoading---1-------:"+url);
            return true;//返回值为true时在WebView中打开，为false时调用浏览器打开  
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            //拦截url 加载回调
            String strUrl = request.getUrl().toString().trim();
            view.loadUrl(strUrl);
            Log.e(Log_tag,"---------shouldOverrideUrlLoading----------:"+strUrl);
              return true;//返回值为true时在WebView中打开，为false时调用浏览器打开  
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }



        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);
            // 回调该方法，处理SSL认证错误
            Log.e(Log_tag,"---------onReceivedSslError----------:");
        }


        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            //网页加载错误(这里进行无网络或错误处理，具体可以根据errorCode的值进行判断，做跟详细的处理。)
        //    super.onReceivedError(view, request, error);
            Log.e(Log_tag,"---------onReceivedError----------:"+request.getUrl().toString());
            //结束当前加载
            closedWebViewActivity();
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            //网页加载错误(这里进行无网络或错误处理，具体可以根据errorCode的值进行判断，做跟详细的处理。)
           // super.onReceivedError(view, errorCode, description, failingUrl);
            //结束当前加载
            Log.e(Log_tag,"----------------------------------errorCode："+errorCode);
            Log.e(Log_tag,"----------------------------------description："+description) ;
            Log.e(Log_tag,"----------------------------------failingUrl："+failingUrl);

            closedWebViewActivityError();
        }
    }


    /**
     * 主要辅助WebView处理Javascript的对话框、
     * 网站图标、网站title、
     * 加载进度等比如
     */
    public  class MyWebChromeClient extends WebChromeClient {
        public MyWebChromeClient() {
            super();
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            //通知应用程序当前网页加载的进度
            //充值handler 信息
            if(newProgress>=100){
                //页面加载完成
                Log.e(Log_tag,"---------onProgressChanged----------:"+view.getUrl());
            }
        }


        /**
         * webview请求得到焦点，发生这个主要是当前webview不是前台状态，是后台webview。
         * @param view
         */
        @Override
        public void onRequestFocus(WebView view) {
            super.onRequestFocus(view);
        }

        @Override
        public void onCloseWindow(WebView window) {
            super.onCloseWindow(window);
            //关闭webview
        }

        /**
         * 警告框
         * @param view
         * @param url
         * @param message
         * @param result
         * @return
         */
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            result.confirm();
            return  true;
        }

        /**
         *  确定框.
         * @param view
         * @param url
         * @param message
         * @param result
         * @return
         */
        @Override
        public boolean onJsConfirm(WebView view, String url, String message, JsResult result) {
            result.confirm();
            return  true;
            //return super.onJsConfirm(view, url, message, result);
        }

        /**
         * 提示框.
         * @param view
         * @param url
         * @param message
         * @param defaultValue
         * @param result
         * @return
         */
        @Override
        public boolean onJsPrompt(WebView view, String url, String message, String defaultValue, JsPromptResult result) {
            result.confirm();
            return true;
           // return super.onJsPrompt(view, url, message, defaultValue, result);
        }

        @Override
        public boolean onJsBeforeUnload(WebView view, String url, String message, JsResult result) {
            return super.onJsBeforeUnload(view, url, message, result);
        }

//        /**
//         * 当前页面请求是否允许进行定位。
//         * @param origin
//         * @param callback
//         */
//        @Override
//        public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
//            super.onGeolocationPermissionsShowPrompt(origin, callback);
//        }

//        /**
//         * 当前一个调用onGeolocationPermissionsShowPrompt() 取消时，通知主机应用请求地理定位权限。隐藏相关的UI。
//         */
//        @Override
//        public void onGeolocationPermissionsHidePrompt() {
//            super.onGeolocationPermissionsHidePrompt();
//        }

//        @Override
//        public void onPermissionRequest(PermissionRequest request) {
//            super.onPermissionRequest(request);
//        }

//        @Override
//        public void onPermissionRequestCanceled(PermissionRequest request) {
//            super.onPermissionRequestCanceled(request);
//        }

        @Override
        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            return super.onConsoleMessage(consoleMessage);
        }

//        /**
//         * Html中，视频（video）控件在没有播放的时候将给用户展示一张“海报”图片（预览图）。
//         * 其预览图是由Html中video标签的poster属性来指定的。
//         * 如果开发者没有设置poster属性, 则可以通过这个方法来设置默认的预览图。
//         * @return
//         */
//        @Override
//        public Bitmap getDefaultVideoPoster() {
//            return super.getDefaultVideoPoster();
//        }

//        /**
//         * 播放视频时，在第一帧呈现之前，需要花一定的时间来进行数据缓冲。
//         * ChromeClient可以使用这个函数来提供一个在数据缓冲时显示的视图。
//         * 例如,ChromeClient可以在缓冲时显示一个转轮动画。
//         * @return
//         */
//        @Override
//        public View getVideoLoadingProgressView() {
//            return super.getVideoLoadingProgressView();
//        }

        /**
         * 获得所有访问历史项目的列表，用于链接着色。
         * @param callback
         */
        @Override
        public void getVisitedHistory(ValueCallback<String[]> callback) {
            super.getVisitedHistory(callback);
        }

        @Override
        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
            return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
        }
    }
}
