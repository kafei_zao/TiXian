package com.ruimayi.sitemoney.beans;

import java.io.Serializable;
import java.util.List;

/**
 * 用户的基本信息
 * Created by admin on 2017/10/15.
 */

public class AdUserParamModel extends BaseModel {
    private String username;
    private String zhifubaoaccount;
    private String zhifubaorealname;
    private String vpndeadline;
    private String vpnstartdate;
    private String phone;
    private String qq;
    private String email;
    private String uvpnname;
    private String vpnpermonthpay;
    private String vpnpayimage;
    private List<ADSiteModel> sites;
    private String shipin;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getZhifubaoaccount() {
        return zhifubaoaccount;
    }

    public void setZhifubaoaccount(String zhifubaoaccount) {
        this.zhifubaoaccount = zhifubaoaccount;
    }

    public String getZhifubaorealname() {
        return zhifubaorealname;
    }

    public void setZhifubaorealname(String zhifubaorealname) {
        this.zhifubaorealname = zhifubaorealname;
    }

    public String getVpndeadline() {
        return vpndeadline;
    }

    public void setVpndeadline(String vpndeadline) {
        this.vpndeadline = vpndeadline;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUvpnname() {
        return uvpnname;
    }

    public void setUvpnname(String uvpnname) {
        this.uvpnname = uvpnname;
    }

    public String getVpnpermonthpay() {
        return vpnpermonthpay;
    }

    public void setVpnpermonthpay(String vpnpermonthpay) {
        this.vpnpermonthpay = vpnpermonthpay;
    }

    public String getVpnpayimage() {
        return vpnpayimage;
    }

    public void setVpnpayimage(String vpnpayimage) {
        this.vpnpayimage = vpnpayimage;
    }

    public List<ADSiteModel> getSites() {
        return sites;
    }

    public void setSites(List<ADSiteModel> sites) {
        this.sites = sites;
    }

    public AdUserParamModel() {
    }

    public String getVpnstartdate() {
        return vpnstartdate;
    }

    public void setVpnstartdate(String vpnstartdate) {
        this.vpnstartdate = vpnstartdate;
    }

    public String getShipin() {
        return shipin;
    }

    public void setShipin(String shipin) {
        this.shipin = shipin;
    }

    @Override
    public String toString() {
        return "AdUserParamModel{" +
                "username='" + username + '\'' +
                ", zhifubaoaccount='" + zhifubaoaccount + '\'' +
                ", zhifubaorealname='" + zhifubaorealname + '\'' +
                ", vpndeadline='" + vpndeadline + '\'' +
                ", vpnstartdate='" + vpnstartdate + '\'' +
                ", phone='" + phone + '\'' +
                ", qq='" + qq + '\'' +
                ", email='" + email + '\'' +
                ", uvpnname='" + uvpnname + '\'' +
                ", vpnpermonthpay='" + vpnpermonthpay + '\'' +
                ", vpnpayimage='" + vpnpayimage + '\'' +
                ", shipin='" + shipin + '\'' +
                '}';
    }
}
