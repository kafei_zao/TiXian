package com.ruimayi.sitemoney.beans;


import java.io.Serializable;
import java.util.List;

/**
 * 网站广告返回的结构体
 * Created by admin on 2017/9/18.
 */

public class ResponeSiteADBaseModel implements Serializable {
    private int c;
    private String m;
    private List<ADSiteModel> d;

    public ResponeSiteADBaseModel() {
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }

    public List<ADSiteModel> getD() {
        return d;
    }

    public void setD(List<ADSiteModel> d) {
        this.d = d;
    }
}
