package com.ruimayi.sitemoney.beans;


/**
 * 广告联盟
 * Created by admin on 2017/9/24.
 */

public class ADSiteModel  extends BaseModel {
    private String name;
    private String url;

    public ADSiteModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
