package com.ruimayi.sitemoney.beans;

/**
 * 登录
 * Created by admin on 2017/9/27.
 */

public class LoginModel extends BaseModel {
    private int status;
    private String vpnname;
    private String vpnpass;
    private int days;
    private String tip;

    public LoginModel() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getVpnname() {
        return vpnname;
    }

    public void setVpnname(String vpnname) {
        this.vpnname = vpnname;
    }

    public String getVpnpass() {
        return vpnpass;
    }

    public void setVpnpass(String vpnpass) {
        this.vpnpass = vpnpass;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    @Override
    public String toString() {
        return "LoginModel{" +
                "status=" + status +
                ", vpnname='" + vpnname + '\'' +
                ", vpnpass='" + vpnpass + '\'' +
                ", days=" + days +
                ", tip='" + tip + '\'' +
                '}';
    }
}
