package com.ruimayi.sitemoney.utils;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * android 外部存储设备
 * Created by admin on 2017/10/31.
 */

public class SystemExternalFileUtils {
   public static boolean mExternalStorageAvailable = false;//是否可以写权限
    public  static boolean mExternalStorageWriteable = false;//是否可以读权限

    /**
     * 判断外部存储的权限
     * @return
     */
    public static final void checkoutExtrenalStatus() {

        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
            Log.e("file_log","-----true-------------true----");
        } else if(Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
            Log.e("file_log","-----true-------------false----");
        } else{
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
            Log.e("file_log","-----false-------------false----");
        }
    }

    /**
     * 获取外部存储的根路径
     * /storage/emulated/0
     * 也就是说api 8以下的版本在操作文件的时候没有专门为私有文件和公共文件的操作提供api支持
     * 。你只能先获取根目录，然后自行想办法。
     * @return
     */
    public static String getPublicExtrenalParentFilePath(String type){
        // Get the directory for the user's public pictures directory.
        String strExtrenalPath = "";
        try {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
                //api 版本大约8
                strExtrenalPath =  Environment.getExternalStoragePublicDirectory(type).getPath();
            } else {
                strExtrenalPath = Environment.getExternalStorageDirectory().getPath();
            }
        }catch (Exception e){

        }
        return strExtrenalPath;
    }

    /**
     * 获取外部存储设备的私有文件
     * /Android/data/<package_name>
     *     也就是说api 8以下的版本在操作文件的时候没有专门为私有文件和公共文件的操作提供api支持。
     *     你只能先获取根目录，然后自行想办法。
     * @return
     */
    public static String  getPrivateExtrenalParentFilePath(Context context,String type){
        String strExtrenalPath = "";
        // Get the directory for the app's private pictures directory.
        try {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
                //api 版本大约8
                strExtrenalPath = context.getExternalFilesDir(type).getAbsolutePath();
            } else {
                strExtrenalPath = Environment.getExternalStorageDirectory().getAbsolutePath();
            }
        }catch (Exception e){

        }
        return strExtrenalPath;
    }

    /**
     *  创建指定文件夹
     * @return
     */
    public static String createDirByExternal(String type,String dirsName){
        String isResult = "";
        try {
            String strDirPath;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
                //api 版本大约8
                strDirPath =  Environment.getExternalStoragePublicDirectory(type).getPath();
            } else {
                strDirPath = Environment.getExternalStorageDirectory().getPath();
            }
            File dirFile = new File(strDirPath,dirsName);
            if(dirFile!=null && dirFile.exists()){
                isResult = dirFile.getPath();
            }else if(dirFile!=null && dirFile.mkdir() ){
                isResult = dirFile.getPath();
            }
        }catch (Exception e){

        }
        Log.e("log_file","-----------------------:"+isResult);
        return isResult;
    }

    /**
     * 获取指定文件夹下的所有文件
     * @param filePath
     * @return
     */
    public static List<String> getParentFiles(String filePath){
        List<String> filePaths = new ArrayList<String>();
        try {
            if (!TextUtils.isEmpty(filePath)) {
                File[] files = new File(filePath).listFiles();
                if(files!=null){
                    for (File file : files) {
                        if (file != null) {
                            filePaths.add(file.getPath());
                        }
                    }
                }
            }
        }catch (Exception e){
            filePaths = null;
        }
        return filePaths;
    }


}
