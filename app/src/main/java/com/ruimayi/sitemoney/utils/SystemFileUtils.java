package com.ruimayi.sitemoney.utils;

import android.content.Context;

import java.io.File;

/**
 * 系统文件管理
 * Created by admin on 2017/9/15.
 */

public class SystemFileUtils {
    /**
     * 获取当前App的缓存目录
     *SDCard/Android/data/你的应用包名/cache/目录，一般存放临时缓存数据
     * @param context
     * @return
     */
    public static String getAppSDCardCacheDirectory(Context context) {
        return context.getExternalCacheDir() != null ? context.getExternalCacheDir().getAbsolutePath() : "cache";
    }
    /**
     * 获取当前App的缓存目录
     * SDCard/Android/data/你的应用的包名/files/ 目录
     * @param context
     * @return
     */
    public static String getAppSDCardFilesDirectory(Context context) {
        return context.getExternalFilesDir(null) != null ? context.getExternalFilesDir(null).getAbsolutePath() : "files";
    }

    /**
     * 获取当前App的缓存目录
     * data/data/你的应用的包名/files/ 目录
     * @param context
     * @return
     */
    public static File getAppFilesDirectory(Context context) {
        File file =null;
        try{
            file = context.getFilesDir();
        }catch (Exception e){

        }
        return file;
    }
    /**
     * 获取当前App的缓存目录
     * data/data/你的应用的包名/files/ 目录
     * @param context
     * @return
     */
    public static File getAppCacheDirectory(Context context) {
        File file = null;
        try{
             file =  context.getCacheDir();
        }catch (Exception e){

        }

        return file;
    }
    /**
     * 获取当前App的目录
     * @param context
     * SDCard/Android/data/你的应用的包名/
     * @return
     */
    public static File getAppSDCardDirectory(Context context){
        File mFileAppDir = null;
        try {
            File file = new File(getAppSDCardCacheDirectory(context));
            if (file != null && file.exists()) {
                mFileAppDir = file.getParentFile();
            } else {
                return null; 
            }
        }catch (Exception e){

        }
        return mFileAppDir;
    }
    /**
     * 获取当前App的目录
     * @param context
     * data/data/你的应用的包名/
     * @return
     */
    public static File getAppDirectory(Context context){
        File mFileAppDir = null;
        try {
            File file = getAppCacheDirectory(context);
            if (file != null && file.exists()) {
                mFileAppDir = file.getParentFile();
            } else {
                return null;
            }
        }catch (Exception e){

        }
        return mFileAppDir;
    }

    /**
     * 通过递归调用删除一个文件夹及下面的所有文件
     *
     * @param file
     */
    public static void deleteFiles(File file) {
        try {

            if (!file.isDirectory()) {//是文件
                file.delete();
            } else {//是目录
                String[] childFilePaths = file.list();
                for (String childFilePath : childFilePaths) {
                    File childFile = new File(file.getAbsolutePath() + File.separator + childFilePath);
                    deleteFiles(childFile);
                }
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
