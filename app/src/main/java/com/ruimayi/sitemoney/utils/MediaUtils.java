package com.ruimayi.sitemoney.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * 媒体工具列
 * Created by admin on 2017/11/1.
 */

public class MediaUtils {

    /**
     * 播放网络问价
     * @param actContext
     * @param url
=     */
    public static void playerNetUrl(Context actContext,String url){
        Intent it = new Intent();
        it.setAction(Intent.ACTION_VIEW);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.parse(url);//此url就是流媒体文件的下载地址
        it.setData(uri);
       // it.setDataAndType(uri, type);//type的值是 "video/*"或者 "audio/*"
        actContext.startActivity(it);
    }

    /**
     * 播放本地视频
     * @param actContext
     * @param url
     * @param type
     */
    public static void playeVideoLocalUrl(Context actContext,String url,String type){
        Intent it = new Intent();
        it.setAction(Intent.ACTION_VIEW);
        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.parse(url);//此url就是流媒体文件的下载地址
         it.setDataAndType(uri, type);//type的值是 "video/*"或者 "audio/*"
        actContext.startActivity(it);
    }
}
