package com.ruimayi.sitemoney.networkUtils;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.gson.Gson;
import com.ruimayi.sitemoney.app.MyApplication;
import com.ruimayi.sitemoney.beans.ResponeSiteADBaseModel;
import com.ruimayi.sitemoney.utils.ToastUtil;


import java.util.HashMap;

/**
 * 网络请求数据
 * Created by admin on 2017/9/8.
 */

public class MyNetworkAsyncTask extends AsyncTask<HashMap<String,String >, Integer, Object > {

    Handler mMyMainHandler;

    public MyNetworkAsyncTask() {
    }
    public MyNetworkAsyncTask(Handler handler){
        mMyMainHandler = handler;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // 被调用后立即执行，一般用来在执行后台任务前对UI做一些标记
    }


    @Override
    protected Object doInBackground(HashMap<String, String>... hashMaps) {
        String strUrl = hashMaps[0].get("web_url");
        return HttpConnectUtils.requestGet(strUrl,hashMaps[1]);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        //在调用publishProgress(Progress... values)时，此方法被执行，直接将进度信息更新到UI组件上。
    }

    @Override
    protected void onPostExecute(Object result) {
        super.onPostExecute(result);
        //当后台操作结束时，此方法将会被调用，计算结果将做为参数传递到此方法中，
        // 直接将结果显示到UI组件上。
        if(result!=null){
            Log.e("http_url","---------onPostExecute-------result:"+result.toString());
            if(result!=null && mMyMainHandler!=null){
                Gson mGson = new Gson();
                ResponeSiteADBaseModel responeSiteADBaseModel = mGson.fromJson(result.toString(), ResponeSiteADBaseModel.class);
                Message message = Message.obtain();
                if(responeSiteADBaseModel!=null && responeSiteADBaseModel.getC()==200){
                    message.what=2001;
                    message.obj = responeSiteADBaseModel.getD();
                }else{
                    //错误
                    message.what=2002;
                    message.obj = responeSiteADBaseModel.getM();
                }
                mMyMainHandler.sendMessage(message);
            }else{
                ToastUtil.showShort(MyApplication.getAppContext(),"登录失败1");
            }
        }else{
            ToastUtil.showShort(MyApplication.getAppContext(),"登录失败2");
        }

    }

    @Override
    protected void onCancelled(Object result) {
        super.onCancelled(result);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
