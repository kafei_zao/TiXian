package com.ruimayi.sitemoney.app;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.ruimayi.sitemoney.utils.SharedPreferencesUtil;
import com.umeng.analytics.MobclickAgent;

/**
 * Created by admin on 2017/9/24.
 */

public class MyApplication extends Application {
    private static Context instance;
    public static String strVpnName;
    public static String strVpnPwd;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = getApplicationContext();
        //Umeng统计
        MobclickAgent.setScenarioType(instance, MobclickAgent.EScenarioType. E_UM_NORMAL);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    /**
     * 获取application 级别的context
     *
     * @return
     */
    public static Context getAppContext() {
        return instance;
    }

    /**
     * 获取用户的vpn 信息
     */
    public static void getUserVpnParams(){
        if(TextUtils.isEmpty(strVpnPwd) || TextUtils.isEmpty(strVpnName)){
            strVpnName = SharedPreferencesUtil.getData(getAppContext(),"user_vpn_name","").toString();
            strVpnPwd = SharedPreferencesUtil.getData(getAppContext(),"user_vpn_pwd","").toString();
        }
    }
}
