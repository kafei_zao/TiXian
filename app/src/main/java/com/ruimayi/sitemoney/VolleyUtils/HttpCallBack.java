package com.ruimayi.sitemoney.VolleyUtils;

/**
 * h
 * @param <T>
 */
public abstract class HttpCallBack<T> {

    public abstract void onSuccess(T data);

    public abstract void onFail(String msg);
}