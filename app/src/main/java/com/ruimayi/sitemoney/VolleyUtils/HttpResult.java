package com.ruimayi.sitemoney.VolleyUtils;

/**
 * http 返回的的泛型结构
 */
public class HttpResult {

    private Object d;
    private int c;
    private String m;

    public Object getD() {
        return d;
    }

    public void setD(Object d) {
        this.d = d;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public String getM() {
        return m;
    }

    public void setM(String m) {
        this.m = m;
    }
}