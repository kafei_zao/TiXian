package com.ruimayi.sitemoney.common;

/**
 * 公共的参数信息
 * Created by admin on 2017/9/6.
 */

public  class CommonParams {
    //private static final String BASE_WEB_URL ="http://www.us02.top/";
    //private static final String BASE_WEB_URL ="http://www.gia1.top/";
    private static final String BASE_WEB_URL ="http://www.ew07.top/";
    //提款
    public static final  String URL_WEB_SITE_MONEY = BASE_WEB_URL+ "zhuanqianapi/api/v1/di/findAccount.do?";

   //登录局接口
    public static final String URL_WEB_LOGIN = BASE_WEB_URL+ "zhuanqianapi/api/v1/di/allowrun.do?";

    //公告地址
    public static final String URL_WEB_NOTICE = BASE_WEB_URL+ "zhuanqianapi/api/v1/di/content.do?";

    //获取视频信息
    public static final String URL_WEB_SHIPIN =BASE_WEB_URL+ "zhuanqianapi/api/v1/di/paramvalue.do?";

    /**
     * 下载文件的
     */
    public static class DownLoadFileParams{
        public static final String FILE_DOWNLOAD_DIRS = "shouzhuan_videos";
    }
}
