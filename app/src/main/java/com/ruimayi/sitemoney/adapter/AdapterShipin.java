package com.ruimayi.sitemoney.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.ruimayi.sitemoneyapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 视频地址适配器
 * Created by admin on 2017/10/31.
 */

public class AdapterShipin extends BaseAdapter {
    private List<String> datas = new ArrayList<String>();
    private Context mContext;

    public AdapterShipin(Context context) {
        mContext = context;
    }
    public void setDatas(List<String> data){
        datas = data;
         notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if(datas==null){
            return 0;
        }else {
            return datas.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView==null){
            viewHolder = new ViewHolder();
            convertView =  LayoutInflater.from(mContext).inflate(R.layout.item_shipin_layout,null);
            viewHolder.setmTextView((TextView) convertView.findViewById(R.id.tv_name));
            convertView.setTag(viewHolder);
        }else{
            viewHolder =(ViewHolder) convertView.getTag();
        }
        //String strName = getItem(position).toString();
        String strName = "手赚视频教程"+(position+1);
        viewHolder.getmTextView().setText(strName);
        return convertView;
    }


    public class ViewHolder{
        private TextView mTextView;

        public ViewHolder() {
        }

        public TextView getmTextView() {
            return mTextView;
        }

        public void setmTextView(TextView mTextView) {
            this.mTextView = mTextView;
        }
    }
}
